(function (OC, window, $, undefined) {
'use strict';

$(document).ready(function () {

	// this notes object holds all our object locations
	var Locations = function (baseUrl) {
	    this._baseUrl = baseUrl;
	    this._locations = [];
	};


	Locations.prototype = {
	    loadAll: function () {
	        var deferred = $.Deferred();
	        var self = this;
	        $.get(this._baseUrl).done(function (locations) {
	            // self._activeNote = undefined;
	            self._locations = locations;	            
	            deferred.resolve();
	            self.renderMarkers();
	        }).fail(function () {
	            deferred.reject();
	        });
	        return deferred.promise();
	    },
	    renderMarkers: function () {
	    	var self = this;

	    	if (self._locations.features){
	    		for (var i = self._locations.features.length - 1; i >= 0; i--) {
	    			var item = self._locations.features[i];
	    			var marker = L.marker( [item.geometry.coordinates[1], item.geometry.coordinates[0]] ).addTo(mymap);
	    			marker.bindPopup( item.properties.popup );
	    		}
	    	}	    		
	    }
	}

	var mapdiv = $('<div id="mapid"></div>');
	$('#app-content-wrapper').append(mapdiv);

	var mymap = L.map('mapid').setView([-21, -46], 3);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
			'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="http://mapbox.com">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap);

	var locations = new Locations(OC.generateUrl('/apps/geolocation/geo'));

	locations.loadAll().done(function () {
	    // view.render();
	    // console.log("done2");
	}).fail(function () {
	    alert('Could not load notes');
	});

});

})(OC, window, jQuery);