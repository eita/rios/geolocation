/*
 * Copyright (c) 2015
 *
 * This file is licensed under the Affero General Public License version 3
 * or later.
 *
 * See the COPYING-README file.
 *
 */

(function(OCA) {

	var Geolocation = function () {
	    this._baseUrl = baseUrl;
	    this._notes = [];
	    this._activeNote = undefined;
	};


	var GeolocationInfoView = OCA.Files.DetailFileInfoView.extend(
		/** @lends OCA.SystemTags.SystemTagsInfoView.prototype */ {

		_rendered: false,

		className: 'geolocationInfoView hidden',

		/**
		 * @type OC.SystemTags.SystemTagsInputField
		 */
		_mapDiv: null,

		_mapClose: null,

		_map: null,

		_marker: null,

		_icon: null,

		_toggleHandle: null,

		_objectId: null,

		_objectName: null,		

		_objectLocation: null,

		initialize: function(options) {
			console.log("initialize");
			var self = this;
			options = options || {};

			this._toggleHandle = $('<span>').addClass('geo-label').text(t('geolocation', 'Geo'));
			this._toggleHandle.prepend($('<span>').addClass('icon icon-tag'));

			// maps
			this._mapDiv = $('<div id="mapid" class="map_popup"></div>');
			this._mapClose = $('<div id="mapclose">&times;</div>');
			this._mapDiv.append(this._mapClose);
			this._mapClose.on('click', function (e) {
				console.log("mapClose");
				self._toggleHandle.click();
				e.stopPropagation();
			});
		},

		_process: function (value) {
			console.log("process");
			var self = this;


			if (this._objectLocation) {
				if (value)
					this._update(value);
				else
					this._destroy(value);
			} else {
				if (value)
					this._create(value);
			}
		},

		_create: function(value){
			console.log("create");
			var self = this;
			var deferred = $.Deferred();
			var data = { 
				geojson: "{ \
					\"type\": \"Feature\", \
					\"geometry\": { \
						\"type\": \"Point\", \
						\"coordinates\": [" + value[1] + ", " + value[0] + "]}, \
					\"properties\": { \
						\"objectId\": " + self._objectId + ", \
						\"file_name\": \"" + self._objectName + "\", \
						\"public_link\": \" \" \
					} }", 
   				lat:value[0], lng:value[1], objectId: self._objectId }; 

			$.ajax({
	            url: OC.generateUrl('/apps/geolocation/geo'),
	            method: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(data)
	        }).done(function () {
				self._loadObjectLocation();
	            deferred.resolve();
	        }).fail(function () {
	            deferred.reject();
	        });

	        return deferred.promise();
		},

		_update: function (value) {
			console.log("update");
			var self = this;
			var deferred = $.Deferred();
			var data = { 
				geojson: "{ \
					\"type\": \"Feature\", \
					\"geometry\": { \
						\"type\": \"Point\", \
						\"coordinates\": [" + value[1] + ", " + value[0] + "]}, \
					\"properties\": { \
						\"objectId\": " + self._objectId + ", \
						\"file_name\": \"" + self._objectName + "\", \
						\"public_link\": \" \" \
					} }", 
   				lat:value[0], lng:value[1], objectId: self._objectId }; 


			$.ajax({
	            url: OC.generateUrl('/apps/geolocation/geo/' + self._objectId),
	            method: 'PUT',
	            contentType: 'application/json',
	            data: JSON.stringify(data)
	        }).done(function () {
	        	self._loadObjectLocation();
	            deferred.resolve();
	        }).fail(function () {
	            deferred.reject();
	        });

	        return deferred.promise();
		},			

		_destroy: function (value) {
			console.log("destroy");
			var self = this;			
			var deferred = $.Deferred();			
			$.ajax({
	            url: OC.generateUrl('/apps/geolocation/geo/' + self._objectId),
	            method: 'DELETE',
	            contentType: 'application/json',
	        }).done(function () {
	        	self._objectLocation = null;
	            deferred.resolve();
	        }).fail(function () {
	            deferred.reject();
	        });

	        return deferred.promise();
		},			

		setFileInfo: function(fileInfo) {
			console.log("setFileInfo");
			var self = this;

			this._objectLocation = null;

			if (fileInfo) {
				this._objectId = fileInfo.id;
				this._objectName = fileInfo.attributes.name;
				this._loadObjectLocation();
				// console.log($('#linkText').val());
				// console.log(fileInfo.attributes.name);
			}

			if (!this._rendered) {
				this.render();
			}

			this.$el.addClass('hidden');
		},

		_loadObjectLocation: function () {
			console.log("loadObjectLocation");
			var deferred = $.Deferred();
	        var self = this;

	        $.get(
	        	OC.generateUrl('/apps/geolocation/geo/' + this._objectId)
	        ).done(function (objectLocation) {
	            self._objectLocation = objectLocation;	         

				if (!self._map){
					self.renderMap();
				}
				if(self._marker){
					self._map.removeLayer(self._marker);
				}
				self._marker = L.marker([self._objectLocation.lat, self._objectLocation.lng])
							.addTo(self._map);
				self._map.setView([self._objectLocation.lat, self._objectLocation.lng]);
				
	            deferred.resolve();
	        }).fail(function () {
	        	if (!self._map){
					self.renderMap();
				}
				if(self._marker){
					self._map.removeLayer(self._marker);
				}
	            deferred.reject();
	        });
	        return deferred.promise();
		},

		/**
		 * Renders this details view
		 */

		render: function() {
			console.log("render");
			var self = this;

			$('#app-sidebar').find('.mainFileInfoView .file-details').append(this._toggleHandle);
			this.$el.append(this._mapDiv);

			this._toggleHandle.off('click');
			this._toggleHandle.on('click', function () {
				console.log("toggleHandle");
				self.$el.toggleClass('hidden');
				if (!self.$el.hasClass('hidden')) {
					// self.$el.find('.geolocationInputField').select2('open');
					// self.renderMap();

					self._map.invalidateSize();
				} 
			});
		},

		renderMap: function () {
			console.log("render map");
			var self = this;

			var initialPos = [-21.0,-46.0];
			if (self._objectLocation)
				initialPos = [self._objectLocation.lat, self._objectLocation.lng];

			self._map = L.map('mapid').setView(initialPos, 4);

			// self._icon = L.icon({
			//     iconUrl: '/apps/geolocation/img/marker-icon.png',
			//     iconSize:     [25, 41], // size of the icon				
			//     iconAnchor:   [12, 41] // point of the icon which will correspond to marker's location
			// });

			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
			'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="http://mapbox.com">Mapbox</a>',
		id: 'mapbox.streets'
			}).addTo(self._map);

			self._map.on('click', function (e) {
			    // console.log(e.latlng.toString());
			    self._process([e.latlng.lat,e.latlng.lng]);
			    if (self._marker)
			    	self._map.removeLayer(self._marker);
			    self._marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(self._map);
			});
		},

		remove: function() {
			console.log("remove");
			this._mapDiv.remove();
			this._toggleHandle.remove();
		}
	});

	console.log("geral");
	OCA.Geolocation.GeolocationInfoView = GeolocationInfoView;

})(OCA)
