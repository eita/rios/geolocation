<?php
namespace OCA\Geolocation\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\ApiController;

use OCA\Geolocation\Service\LocationService;
use OCA\Ldapusermanagement;


use OC\Files\Node\Folder;
use OC_Files;
use OC_Util;
use OCA\FederatedFileSharing\FederatedShareProvider;
use OCP\Defaults;
use OCP\IL10N;
use OCP\Template;
use OCP\Share;
use OCP\AppFramework\Controller;
use OCP\IURLGenerator;
use OCP\IConfig;
use OCP\ILogger;
use OCP\IUserManager;
use OCP\ISession;
use OCP\IPreview;
use OCA\Files_Sharing\Activity\Providers\Downloads;
use OCP\Files\IRootFolder;
use OCP\Share\Exceptions\ShareNotFound;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class LocationApiController extends ApiController {

    private $service;
    private $objectId;

    use Errors;

     public function __construct($appName,
            IRequest $request,
            IConfig $config,
            IURLGenerator $urlGenerator,
            IUserManager $userManager,
            ILogger $logger,
            \OCP\Activity\IManager $activityManager,
            \OCP\Share\IManager $shareManager,
            ISession $session,
            IPreview $previewManager,
            IRootFolder $rootFolder,
            FederatedShareProvider $federatedShareProvider,
            EventDispatcherInterface $eventDispatcher,
            IL10N $l10n,
            \OC_Defaults $defaults,
            LocationService $service ) {

                    parent::__construct($appName, $request);

                    $this->config = $config;
                    $this->urlGenerator = $urlGenerator;
                    $this->userManager = $userManager;
                    $this->previewManager = $previewManager;
                    $this->shareManager = $shareManager;
                    $this->service = $service;
            }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     * @PublicPage
     */
    public function index() {
        \OC::$server->getLogger()->error(">>>index API controler", array('app' => 'geolocation'));        

        $json['type'] = "FeatureCollection";
        $json['features'] = NULL;

        foreach ($this->service->findAllPublicGeojson() as $item) {
            // \OC::$server->getLogger()->error( print_r( $item, true) , array('app' => 'geolocation'));
            /* insert public link into json outuput */
            $geoarray = json_decode($item['geojson'], TRUE);
            $geoarray['properties']['file_name'] = $item['name'];

            $rootURL = \OC::$server->getURLGenerator()->getAbsoluteURL( NULL );

            $share = $this->shareManager->getShareByToken($item['token']);
            if ( $this->previewManager->isMimeSupported($share->getNode()->getMimetype()) ) {
                $thumbURL = $rootURL . "/index.php/apps/files_sharing/ajax/publicpreview.php?x=64&y=64&" . "t=" . $item['token'];
            } else {
                $thumbURL = $rootURL . \OC::$server->getMimeTypeDetector()->mimeTypeIcon($share->getNode()->getMimetype());
            }

            $geoarray['properties']['public_link'] = $rootURL . "/index.php/s/" . $item['token'];
            $geoarray['properties']['popup'] = 
                    "<img src='$thumbURL' style='float:left ; margin-right: 10px; height: 64px'>" . 
                    "<div style='margin-bottom:25px'>Nome: " . $geoarray['properties']['file_name'] . "<br> " . 
                    "Link: <a href='" . $geoarray['properties']['public_link'] . "'>" . $geoarray['properties']['public_link'] . "</a></div>";

            $json['features'][] = $geoarray;
        }
        
        return new JSONResponse ( $json );
    }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     * @PublicPage
     */
    public function users() {
        \OC::$server->getLogger()->error(">>>index API controler", array('app' => 'geolocation'));

        $ds = \OCA\Ldapusermanagement\LDAPConnect::bind( );
        $dn = \OCP\Config::getAppValue( 'user_ldap', 'ldap_base_users', '' );

        $filter = "objectClass=inetOrgPerson";
        $justthese = array( "sn", "displayname", "mail", "street" );
        $sr = ldap_search ( $ds , $dn , $filter , $justthese );
        $users = ldap_get_entries( $ds, $sr );

        \OC::$server->getLogger()->error( $dn , array('app' => 'geolocation'));

        $json['type'] = "FeatureCollection";
        $json['features'] = NULL;

        foreach ($users as $user) {

            $i = rand(1,10) * 0.01;

            $geoarray['type'] = 'Feature';
            $geoarray['geometry']['type'] = 'Point';
            $geoarray['geometry']['coordinates'] = array(-42.36328125+$i,-19.145168196205+$i);
            $geoarray['properties']['displayName'] = $user['displayname'][0];

            $json['features'][] = $geoarray;
        }
        
        return new JSONResponse ( $json );
    }


    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     *
     * @param int $objectId
     */
    public function show($objectId) {
        \OC::$server->getLogger()->error(">>>show API controler - $objectId >>>>", array('app' => 'geolocation'));
        return $this->handleNotFound(function () use ($objectId) {
            return $this->service->find($objectId);
        });
    }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     *
     * @param string $lat
     * @param string $lng
     * @param string $geojson
     */
    public function create($title, $content) {
        return $this->service->create($lat, $lng, $geojson, $this->objectId);
    }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     *
     * @param int $id
     * @param string $lat
     * @param string $lng
     * @param string $geojson
     */
    public function update($id, $lat, $lng, $geojson) {
        return $this->handleNotFound(function () use ($id, $lat, $lng, $geojson) {
            return $this->service->update($id, $lat, $lng, $geojson, $this->objectId);
        });
    }

    /**
     * @CORS
     * @NoCSRFRequired
     * @NoAdminRequired
     *
     * @param int $id
     */
    public function destroy($id) {
        return $this->handleNotFound(function () use ($id) {
            return $this->service->delete($id, $this->objectId);
        });
    }

}