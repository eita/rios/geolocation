<?php
namespace OCA\Geolocation\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\ApiController;

use OCA\Geolocation\Service\LocationService;
use OCA\Ldapusermanagement;


use OC\Files\Node\Folder;
use OC_Files;
use OC_Util;
use OCA\FederatedFileSharing\FederatedShareProvider;
use OCP\Defaults;
use OCP\IL10N;
use OCP\Template;
use OCP\Share;
use OCP\AppFramework\Controller;
use OCP\IURLGenerator;
use OCP\IConfig;
use OCP\ILogger;
use OCP\IUserManager;
use OCP\ISession;
use OCP\IPreview;
use OCA\Files_Sharing\Activity\Providers\Downloads;
use OCP\Files\IRootFolder;
use OCP\Files\IMimeTypeLoader;
use OCP\Share\Exceptions\ShareNotFound;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class GeolocationController extends Controller {

    /** @var IMimeTypeLoader */
    protected $mimetypeLoader;

    private $service;
    private $objectId;

    use Errors;

     public function __construct($appName,
            IRequest $request,
            IConfig $config,
            IURLGenerator $urlGenerator,
            IUserManager $userManager,
            ILogger $logger,
            \OCP\Activity\IManager $activityManager,
            \OCP\Share\IManager $shareManager,
            ISession $session,
            IPreview $previewManager,
            IRootFolder $rootFolder,
            FederatedShareProvider $federatedShareProvider,
            EventDispatcherInterface $eventDispatcher,
            IL10N $l10n,
            \OC_Defaults $defaults,
            LocationService $service ) {

                    parent::__construct($appName, $request);

                    $this->config = $config;
                    $this->urlGenerator = $urlGenerator;
                    $this->userManager = $userManager;
                    $this->previewManager = $previewManager;
                    $this->shareManager = $shareManager;
                    $this->service = $service;
                    $this->mimetypeLoader = \OC::$server->getMimeTypeLoader();
            }

    /**
     * @NoAdminRequired
     */
    public function index() {
        \OC::$server->getLogger()->error(">>>index API controler", array('app' => 'geolocation'));        

        $json['type'] = "FeatureCollection";
        $json['features'] = NULL;

        foreach ($this->service->findAllGeojson() as $item) {
            // \OC::$server->getLogger()->error( print_r( $item, true) , array('app' => 'geolocation'));
            /* insert public link into json outuput */
            $geoarray = json_decode($item['geojson'], TRUE);
            $geoarray['properties']['file_name'] = $item['name'];

            $rootURL = \OC::$server->getURLGenerator()->getAbsoluteURL( NULL );            

            if ( $this->previewManager->isMimeSupported( $this->mimetypeLoader->getMimetypeById($item['mimetype'] )) ) {
                // $file = \OC::$server->getRootFolder()->getById($item['fileid']);
                // foreach ($file as $f) {
                //     $preview = $this->previewManager->getPreview($f);
                //     \OC::$server->getLogger()->error( $preview->getName(),  array('app' => 'geolocation'));
                // }
                // $this->previewManager->getPreview($file);                 
                
                $thumbURL = $rootURL . "index.php/core/preview.png?file=%2F" . substr($item['path'], 6) . "&x=64&y=64";
            } else {
                $thumbURL = $rootURL . \OC::$server->getMimeTypeDetector()->mimeTypeIcon( $this->mimetypeLoader->getMimetypeById($item['mimetype'] ));
            }

            $geoarray['properties']['private_link'] = $rootURL . "remote.php/webdav/" . substr($item['path'], 6);
            $geoarray['properties']['popup'] = 
                    "<img src='$thumbURL' style='float:left ; margin-right: 10px; height: 64px'>" . 
                    "<div style='margin-bottom:25px'>Nome: " . $geoarray['properties']['file_name'] . "<br> " . 
                    "Link: <a href='" . $geoarray['properties']['private_link'] . "'>" . $geoarray['properties']['private_link'] . "</a></div>";

            $json['features'][] = $geoarray;
        }
        
        return new JSONResponse ( $json );
    }

    // /**
    //  * @NoAdminRequired
    //  */
    // public function index() {
    //     return LocationApiController::index();
    //     return new DataResponse($this->service->findAll());
    // }

    /**
     * @NoAdminRequired
     *
     * @param int $id
     */
    public function show($objectId) {
        // \OC::$server->getLogger()->error(">>>show controler - $objectId >>>>", array('app' => 'geolocation'));
        return $this->handleNotFound(function () use ($objectId) {
            return $this->service->find($objectId);
        });
    }

    /**
     * @NoAdminRequired
     *
     * @param string $lat
     * @param string $lng
     * @param string $geojson     
     * @param int $objectId
     */
    public function create($lat, $lng, $geojson, $objectId) {
        // \OC::$server->getLogger()->error(">>>create controler - $lat $lng $objectId >>>>", array('app' => 'geolocation'));
        return $this->service->create($lat, $lng, $geojson, $objectId);
    }

    /**
     * @NoAdminRequired
     *
     * @param int $id
     * @param string $lat
     * @param string $lng
     * @param string $geojson
     * @param int $objectId
     */
    public function update($lat, $lng, $objectId, $geojson) {
        \OC::$server->getLogger()->error(">>>update controler - $lat $lng $objectId >>>>", array('app' => 'geolocation'));
        return $this->handleNotFound(function () use ($lat, $lng, $geojson, $objectId) {
            return $this->service->update($lat, $lng, $geojson, $objectId);
        });
    }

    /**
     * @NoAdminRequired
     *
     * @param int $id
     */
    public function destroy($objectId) {
        return $this->handleNotFound(function () use ($objectId) {
            return $this->service->delete($objectId);
        });
    }

}