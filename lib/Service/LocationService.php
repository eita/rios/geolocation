<?php
namespace OCA\Geolocation\Service;

use Exception;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;

use OCA\Geolocation\Db\Location;
use OCA\Geolocation\Db\LocationMapper;


class LocationService {

    private $mapper;

    public function __construct(LocationMapper $mapper){
        $this->mapper = $mapper;
    }

    public function findAll() {
        return $this->mapper->findAll();
    }

    public function findAllGeojson() {
        return $this->mapper->findAllGeojson();
    }

    public function findAllPublicGeojson() {
        return $this->mapper->findAllPublicGeojson();
    }

    private function handleException ($e) {
        if ($e instanceof DoesNotExistException ||
            $e instanceof MultipleObjectsReturnedException) {
            throw new NotFoundException($e->getMessage());
        } else {
            throw $e;
        }
    }

    public function find($objectId) {
         \OC::$server->getLogger()->error("<<<find service $objectId<<<<", array('app' => 'geolocation'));

        try {
            return $this->mapper->find($objectId);

        // in order to be able to plug in different storage backends like files
        // for instance it is a good idea to turn storage related exceptions
        // into service related exceptions so controllers and service users
        // have to deal with only one type of exception
        } catch(Exception $e) {
            $this->handleException($e);
        }
    }

    public function create($lat, $lng, $geojson, $objectId) {
         // \OC::$server->getLogger()->error("<<<create service $objectId<<<<", array('app' => 'geolocation'));
         $location = new Location();
         $location->setLat($lat);
         $location->setLng($lng);
         $location->setGeojson($geojson);
         $location->setObjectId($objectId);
         return $this->mapper->insert($location);
    }


    public function update($lat, $lng, $geojson, $objectId) {
        \OC::$server->getLogger()->error(">>>update service - $lat $lng $objectId >>>>", array('app' => 'geolocation'));
        try {
            $location = $this->mapper->find($objectId);
            $location->setLat($lat);
            $location->setLng($lng);
            $location->setGeojson($geojson);
            return $this->mapper->update($location);
        } catch(Exception $e) {
            $this->handleException($e);
        }
    }

    public function delete($objectId) {
        try {
            $location = $this->mapper->find($objectId);
            $this->mapper->delete($location);
            return $location;
        } catch(Exception $e) {
            $this->handleException($e);
        }
    }

}