<?php
namespace OCA\Geolocation\Db;

use JsonSerializable;

use OCP\AppFramework\Db\Entity;

class Location extends Entity implements JsonSerializable {

    protected $objectId;
    protected $lat;
    protected $lng;
    public $geojson;

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'objectId' => $this->objectId,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'geojson' => $this->geojson
        ];
    }
}