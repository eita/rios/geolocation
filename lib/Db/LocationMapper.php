<?php
namespace OCA\Geolocation\Db;

use OCP\IDbConnection;
use OCP\AppFramework\Db\Mapper;

class LocationMapper extends Mapper {

    public function __construct(IDbConnection $db) {
        parent::__construct($db, 'geolocation_location', '\OCA\Geolocation\Db\Location');
    }

    public function find($objectId) {
        $sql = 'SELECT * FROM *PREFIX*geolocation_location WHERE object_id = ?';
        return $this->findEntity($sql, [$objectId]);
    }

    public function findAll() {
        $sql = 'SELECT * FROM *PREFIX*geolocation_location';
        return $this->findEntities($sql);
    }

    public function findAllPublicGeojson() {
        /* this query returns all geolocated files that are publicly shared, with the share token */
        $sql = 'SELECT *PREFIX*filecache.name, *PREFIX*share.token, *PREFIX*geolocation_location.geojson FROM *PREFIX*share INNER JOIN *PREFIX*geolocation_location ON *PREFIX*geolocation_location.object_id=*PREFIX*share.file_source INNER JOIN *PREFIX*filecache ON *PREFIX*filecache.fileid=*PREFIX*share.file_source' ;

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = NULL;
        while ($row = $stmt->fetch()){
            $result[] = $row;
        }
        $stmt->closeCursor();
        return $result;
    }

    public function findAllGeojson() {
        /* this query returns all geolocated files that are publicly shared, with the share token */
        $sql = 'SELECT *PREFIX*filecache.name, *PREFIX*filecache.mimetype, *PREFIX*filecache.path, *PREFIX*filecache.fileid, *PREFIX*geolocation_location.geojson FROM *PREFIX*filecache INNER JOIN *PREFIX*geolocation_location ON *PREFIX*geolocation_location.object_id=*PREFIX*filecache.fileid';

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = NULL;
        while ($row = $stmt->fetch()){
            $result[] = $row;
        }
        $stmt->closeCursor();
        return $result;
        // return "bla";
    }

}