<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\Geolocation\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'routes' => [
	   ['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
	   ['name' => 'page#do_echo', 'url' => '/echo', 'verb' => 'POST'],
	   ['name' => 'geolocation#index', 'url' => '/geo', 'verb' => 'GET'],
	   ['name' => 'geolocation#create', 'url' => '/geo', 'verb' => 'POST'],
	   ['name' => 'geolocation#show', 'url' => '/geo/{objectId}', 'verb' => 'GET'],
	   ['name' => 'geolocation#update', 'url' => '/geo/{objectId}', 'verb' => 'PUT'],
       ['name' => 'geolocation#destroy', 'url' => '/geo/{objectId}', 'verb' => 'DELETE'],
       ['name' => 'location_api#show', 'url' => '/api/0.1/geo/{objectId}', 'verb' => 'GET'],
       ['name' => 'location_api#index', 'url' => '/api/0.1/geo/', 'verb' => 'GET'],
       ['name' => 'location_api#users', 'url' => '/api/0.1/geo_users/', 'verb' => 'GET'],       
    ]
];