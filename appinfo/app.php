<?php

use OCP\AppFramework\App;

$app = new App('geolocation');
$container = $app->getContainer();

$container->query('OCP\INavigationManager')->add(function () use ($container) {
	$urlGenerator = $container->query('OCP\IURLGenerator');
	$l10n = $container->query('OCP\IL10N');
	return [
		// the string under which your app will be referenced in Nextcloud
		'id' => 'geolocation',

		// sorting weight for the navigation. The higher the number, the higher
		// will it be listed in the navigation
		'order' => 10,

		// the route that will be shown on startup
		'href' => $urlGenerator->linkToRoute('geolocation.page.index'),

		// the icon that will be shown in the navigation
		// this file needs to exist in img/
		'icon' => $urlGenerator->imagePath('geolocation', 'maps.svg'),

		// the title of your application. This will be used in the
		// navigation or on the settings page of your app
		'name' => $l10n->t('Geolocation'),
	];
});

$eventDispatcher = \OC::$server->getEventDispatcher();
$eventDispatcher->addListener(
	'OCA\Files::loadAdditionalScripts',
	function() {
		\OCP\Util::addScript('geolocation', 'filesplugin');
		\OCP\Util::addScript('geolocation', 'script');
		\OCP\Util::addScript('geolocation', 'leaflet');
		\OCP\Util::addStyle('geolocation', 'style');
		\OCP\Util::addStyle('geolocation', 'leaflet');		
	}
);

if(class_exists('\\OCP\\AppFramework\\Http\\EmptyContentSecurityPolicy')) {
	$manager = \OC::$server->getContentSecurityPolicyManager();
	$policy = new \OCP\AppFramework\Http\EmptyContentSecurityPolicy();
	$policy->addAllowedImageDomain('https://api.tiles.mapbox.com');
	$policy->addAllowedScriptDomain('\'self\'');
	$manager->addDefaultPolicy($policy);
}
